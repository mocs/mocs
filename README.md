# \_README

This is the place to start for working in the _mocs_ project!

# Get started

Easiest way to get started is to execute the script `./get_all_repos.sh` which
will set you up!

The script will `git clone` all repos to the folder _above_ this one.
