#!/usr/bin/env bash

# Use this file to get all repos.

# Always die on error
set -e

# Get the folder name of _this_ script
SRCDIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
# Get the folder of the executing instance
TARGET=$(pwd)
source MANIFEST

# Setup
RED='\033[31;1m'
GREEN='\033[32;1m'
YELLOW='\033[33;1m'
BLUE='\033[34;1m'
MAGENTA='\033[35;1m'
CYAN='\033[36;1m'
red='\033[0;31m'
green='\033[0;32m'
yellow='\033[0;33m'
blue='\033[0;34m'
magenta='\033[0;35m'
cyan='\033[0;36m'
NC='\033[0m'

#Check arguments
while [[ $# -gt 0 ]]; do
  key="$1"
  case $key in
    -d|--debug)
      DEBUG=1
      printf "${CYAN}"
      printf "Running in debug mode.\n"
      shift
      ;;
    -c|--clean)
      CLEAN=1
      printf "${CYAN}"
      printf "Will remove existing repositories.\n"
      shift
      ;;
    *)
      printf "${CYAN}"
      printf "Unknown command given. (${1})\n"
      printf "${RED}"
      printf "Exiting.\n"
      printf "${NC}"
      exit
  esac
done

# Sanity checks
printf "${MAGENTA}"
printf "###############################################################################\n"
printf "### Executing sanity checks!                                                ###\n"
printf "###############################################################################\n"
printf "${NC}"
printf "${cyan}"
printf "     Checking that we are executing in self..."
if [[ "${TARGET}" != "${SRCDIR}" ]]; then
  printf "\r"
  _exit_cmd "This script cannot be executed in this folder." 1
else
  printf "\r${GREEN}OK\n${NC}"
fi
printf "${NC}"

cd ..

for repo in "${repos[@]}"; do
  if ! [[ $repo == *"https"* ]]; then
    repo_git=$(echo "$repo" | sed 's#git@gitlab.com:#/#')
    folder=$(echo "$repo_git" | awk -F'/mocs/' '{print $2}')
  else
    folder=$(echo "$repo" | awk -F'/mocs/' '{print $2}')
  fi
  name=$(echo "$folder" | awk -F'/' '{print $NF}' | awk -F'.' '{print $1}')
  folder=$(echo "$folder" | awk -F'/' '{for (i=1; i<NF; i++) print $i}' | tr '\n' '/')
  printf "Cloning ${cyan}$repo${NC} into ${BLUE}../$folder$name${NC}\n"
  if [[ -d "$folder$name" ]]; then
    printf "${MAGENTA}Found existing folder!${NC}"
    if ! [[ -z $CLEAN ]]; then
      printf "${cyan} Removing...${NC}\n"
      rm -fr "$folder$name"
    else
      printf "${RED} Exiting!${NC}\n"
      exit 1
    fi
  fi
  if [[ ${#folder} -gt 0 ]]; then
    mkdir -p ${folder}
    pushd $folder
    git clone "${repo}"
    pushd $name
    git submodule update --init --recursive
    if ! [ -Z $git_user ]; then
      printf "${cyan}Setting git user (${blue}$git_user${cyan}) and email (${blue}$git_email${cyan}).${NC}\n"
      git config user.name "${git_user}"
      git config user.email "${git_email}"
    fi
    popd
    popd
  else
    git clone "${repo}"
    pushd $name
    git submodule update --init --recursive
    if ! [ -Z $git_user ]; then
      printf "${cyan}Setting git user (${blue}$git_user${cyan}) and email (${blue}$git_email${cyan}).${NC}\n"
      git config user.name "${git_user}"
      git config user.email "${git_email}"
    fi
    popd
  fi
done
